/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framework.configuration.xml;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Ayra
 */
public class MainWithXmlConfig {

    public static void main(String[] args) {
        AbstractApplicationContext ctx = new ClassPathXmlApplicationContext("springXMLConfig.xml");
        HelloService saluda = ctx.getBean("saludaService", HelloService.class);
        saluda.saludar();
        ctx.close();
    }

}
