/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framework.configuration.java;

import com.framework.configuration.xml.HelloService;
import com.framework.configuration.xml.HelloServiceImpl;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;

/**
 *
 * @author Ayra
 */
@Configuration
public class SpringConfiguration {
    
    @Bean
    public HelloService saludaService(){
        return new HelloServiceImpl();
    }
    
}
