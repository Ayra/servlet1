/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framework.configuration.annotaciones;

import org.springframework.stereotype.Service;

/**
 *
 * @author Ayra
 */
@Service("saludaService")
public class HelloServiceImpl implements HelloService{

    @Override
    public void saludar() {
        System.out.println("[**[Spring configurado mediante anotaciones]**]");
    }
    
}
