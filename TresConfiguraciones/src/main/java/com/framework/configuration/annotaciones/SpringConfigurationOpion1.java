/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framework.configuration.annotaciones;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author Ayra
 */
@Configuration
@ComponentScan("com.framework.configuration.annotaciones")
public class SpringConfigurationOpion1 {
    
}
