/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.framework.configuration.annotaciones;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

/**
 *
 * @author Ayra
 */
public class MainWithJavaConfig {
    public static void main(String[] args) {
       AbstractApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfigurationOpion1.class);  
        
        HelloService saluda = ctx.getBean("saludaService",HelloService.class);
        saluda.saludar();
        ctx.close();
    }
}
